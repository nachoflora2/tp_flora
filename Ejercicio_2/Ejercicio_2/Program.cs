﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class Persona
    {
        private string nombre = " ";
        private int edad = 0;
        private int peso = 0;
        private int altura = 0;
        private string sexo = "H";

        public Persona(string nombre, int edad, string sexo, int peso, int altura)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.peso = peso;
            this.altura = altura;
            this.sexo = sexo;

        }
        public string Nombre
        {
            set { this.Nombre = nombre; }
        }
        public int Edad
        {
            set { this.Edad = edad; }
        }
        public int Peso
        {
            set { this.Peso = peso; }
        }
        public int Altura
        {
            set { this.Altura = altura; }
        }
        public string Sexo
        {
            set { this.Sexo = sexo; }
        }
        public void Comprobarsexo()
        {
            if (sexo != "H" && sexo != "M")
            {
                this.sexo = "H";
            }
        }
        public bool MayorDeEdad()
        {
            bool mayor = false;
            if (edad >= 18)
            {
                mayor = true;
            }
            return mayor;
        }
        public int CalcularIMC()
        {
            int pesoideal = peso / (altura * altura);
            if (pesoideal >= 20 && pesoideal <= 25)
            {
                return peso;
            }
            else if (pesoideal < 20)
            {
                return -1;
            }
            else
            {
                return 1;
            }
        }

    class Ejecutable
    {
        static Persona p;
        static string nom, sex;
        static int edad, peso, alt;

        static void Main(string[] args)
        {
            Console.Write("Ingrese un nombre: ");
            nom = Console.ReadLine();
            Console.Write("Ingrese la edad: ");
            edad = int.Parse(Console.ReadLine());
            Console.Write("Ingrese el sexo (H o M): ");
            sex = Console.ReadLine();
            Console.Write("Ingrese el peso: ");
            peso = int.Parse(Console.ReadLine());
            Console.Write("Ingrese la altura: ");
            alt = int.Parse(Console.ReadLine());
            p = new Persona(nom, edad, sex, peso, alt);

            if (p.MayorDeEdad())
            {
                Console.WriteLine("La persona es mayor de edad");
            }
            else
            {
                Console.WriteLine("La persona es menor de edad");
            }
                Console.ReadKey();
            }
        }

    }
}

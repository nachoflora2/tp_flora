﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio5
{
    interface Entregable
    {
        bool Entregar();
        bool devolver();
        bool isEntregado();
        int compareTo(object a);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio5
{
    class Videojuegos : Entregable
    {
        private string titulo;
        private string genero;
        private string compañia;
        private int horasestimadas = 10;
        private bool entregado = false;

        public Videojuegos() {}

        public Videojuegos(string titulo,int horasestimadas) {
            this.titulo = titulo;
            this.horasestimadas = horasestimadas;
        }

        public Videojuegos(string titulo, string genero, string compañia,int horasestimadas)
        {
            this.titulo = titulo;
            this.horasestimadas = horasestimadas;
            this.genero = genero;
            this.compañia = compañia;
        }
        public string Titulo
        {
            get { return titulo; }
            set { this.Titulo = titulo; }
        }
        public int Horasestimadas
        {
            get { return horasestimadas; }
            set { this.Horasestimadas = horasestimadas; }
        }
        public string Genero
        {
            get { return genero; }
            set { this.Genero = genero; }
        }
        public string Compañia
        {
            get { return compañia; }
            set { this.Compañia = compañia; }
        }
        public bool Entregar()
        {
            Console.WriteLine("Se entrego el Videojuego");
            entregado = true;
            return entregado;
        }
        public bool devolver()
        {
            Console.WriteLine("Se devolvio el Videojuego");
            entregado = false;
            return entregado;
        }
        public bool isEntregado()
        {
            if (entregado)
            {
                return true;
            }
            return false;
        }
        public int compareTo(object a)
        {
            Videojuegos objvideo = (Videojuegos)(a);
            if (horasestimadas < objvideo.Horasestimadas)
            {
                return -1;
            }
            else if (horasestimadas > objvideo.Horasestimadas)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

    }
}

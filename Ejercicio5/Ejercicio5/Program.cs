﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio5
{
    class Series : Entregable
    {
        private string titulo;
        private string creador;
        private int numtemporada = 3;
        private string genero;
        private bool entregado = false;

        public Series() { }

        public Series(string titulo, string creador)
        {
            this.titulo = titulo;
            this.creador = creador;
        }

        public Series(string titulo, string creador, int numtemporada, string genero)
        {
            this.titulo = titulo;
            this.creador = creador;
            this.numtemporada = numtemporada;
            this.genero = genero;
        }
        public string Titulo
        {
            get { return titulo; }
            set { this.Titulo = titulo; }
        }
        public string Creador
        {
            get { return creador; }
            set { this.Creador = creador; }
        }
        public int Numtemporada
        {
            get { return numtemporada; }
            set { this.Numtemporada = numtemporada; }
        }
        public string Genero
        {
            get { return genero; }
            set { this.Genero = genero; }
        }
        public bool Entregar()
        {
            Console.WriteLine("Se entrego la Serie");
            entregado = true;
            return entregado;
        }
        public bool devolver()
        {
            Console.WriteLine("Se devolvio la Serie");
            entregado = false;
            return entregado;
        }
        public bool isEntregado()
        {
            if (entregado)
            {
                return true;
            }
            return false;
        }
        public int compareTo(object a)
        {
            Series objseries = (Series)(a);
            if (numtemporada < objseries.Numtemporada)
            {
                return -1;
            } else if (numtemporada > objseries.Numtemporada)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

    }
    class Ejecutable
    {

        static void Main(string[] args)
        {
            Series[] arraySeries = new Series[5];
            Videojuegos[] arrayVideojuegos = new Videojuegos[5];
            arraySeries[0] = new Series();
            arraySeries[1] = new Series("Juego de tronos", "George R. R. Martin ",8,"Drama y Fantasia");
            arraySeries[2] = new Series("Los Simpsons", "Matt Groening", 25, "Humor");
            arraySeries[3] = new Series("Padre de familia", "Seth MacFarlane", 12, "Humor");
            arraySeries[4] = new Series("Breaking Bad", "Vince Gilligan", 5, "Thriller");

            arrayVideojuegos[0] = new Videojuegos();
            arrayVideojuegos[1] = new Videojuegos("Assasin creed 2", "Aventura", "EA",30);
            arrayVideojuegos[2] = new Videojuegos("God of war 3","Accion y Aventura","Santa Monica",20);
            arrayVideojuegos[3] = new Videojuegos("Super Mario 3DS", "Plataforma","Nintendo",30);
            arrayVideojuegos[4] = new Videojuegos("The last of us","Accion,aventura y Horror","Naughty dog",25);

            arrayVideojuegos[1].Entregar();
            Console.WriteLine(arrayVideojuegos[1].Titulo);
            arrayVideojuegos[4].Entregar();
            Console.WriteLine(arrayVideojuegos[4].Titulo);
            arraySeries[1].Entregar();
            Console.WriteLine(arraySeries[1].Titulo);
            arraySeries[2].Entregar();
            Console.WriteLine(arraySeries[2].Titulo);

            int entregados = 0;

            for (int i = 0; i < arraySeries.Length; i++)
            {
                if (arrayVideojuegos[i].isEntregado())
                {
                    entregados += 1;
                    arrayVideojuegos[i].devolver();
                }

                if (arraySeries[i].isEntregado())
                {
                    entregados += 1;
                    arraySeries[i].devolver();

                }
            }

            Console.ReadLine();
        }
    }
}

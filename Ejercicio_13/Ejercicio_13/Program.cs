﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_13
{
    public class Producto
    {
        public string nombre;
        public int precio;

        public Producto(string nombre, int precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }
        public string Nombre
        {
            get { return nombre; }
            set { this.Nombre = nombre; }
        }
        public int Precio
        {
            get { return precio; }
            set { this.Precio = precio; }
        }
        public double calcular(int cantidad)
        {
            return precio * cantidad;
        }
    }
    class Ejecutable
    {
        static void Main(string[] args)
        {
            Producto[] productos = new Producto[3];

            productos[0] = new Producto("producto 1",10);
            productos[1] = new Perecedero("producto 2", 20, 1);
            productos[2] = new Noperecedero("producto 3",50, "tipo 1");

            double total = 0;
            for (int i = 0; i < productos.Length; i++)
            {
                total += productos[i].calcular(5);
            }

            Console.WriteLine("el total es " + total);
            Console.ReadLine();

        }

    }

}
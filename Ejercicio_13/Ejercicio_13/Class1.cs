﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_13
{
    public class Perecedero : Producto
    {
        private int diascaducar;

        public Perecedero(string nombre, int precio, int diascaducar)
            : base(nombre,precio)
        {
            this.diascaducar = diascaducar;
        }
        public int Diascaducar
        {
            get { return diascaducar; }
            set { this.Diascaducar = diascaducar; }
        }
        public  new int calcular(int cantidad)
        {

            int precioFinal = calcular(cantidad);

            switch (diascaducar)
            {
                case 1:
                    precioFinal /= 4;
                    break;
                case 2:
                    precioFinal /= 3;
                    break;
                case 3:
                    precioFinal /= 2;
                    break;
            }

            return precioFinal;

        }
    }
    public class Noperecedero : Producto
    {
        private string tipo;

        public Noperecedero(string nombre, int precio,string tipo)
            : base(nombre, precio)
        {
            this.tipo = tipo;
        }
        public string Tipo
        {
            get { return tipo; }
            set { this.Tipo = tipo; }
        }
    }
}

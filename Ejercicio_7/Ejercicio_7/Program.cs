﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_7
{
    class Raices
    {
        private int a;
        private int b;
        private int c;

        public Raices(int a, int b, int c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        public int Discriminante
        {
            get { return ((b*b)-(4*a*c)); }
        }
        public bool TieneRaices()
        {
            return Discriminante > 0;
        }
        public bool TieneRaiz()
        {
            return Discriminante == 0;
        }
        private void ObtenerRaices()
        {

            double x1 = (-b + Math.Sqrt(Discriminante)) / (2 * a);
            double x2 = (-b - Math.Sqrt(Discriminante)) / (2 * a);

            Console.WriteLine("Solucion X1");
            Console.WriteLine(x1);
            Console.WriteLine("Solucion X2");
            Console.WriteLine(x2);
        }

        public void ObtenerRaiz()
        {

            double x = (-b) / (2 * a);

            Console.WriteLine("Unica solucion");
            Console.WriteLine(x);

        }
        public void calcular()
        {

            if (TieneRaices())
            {
                ObtenerRaices();
            }
            else if (TieneRaiz())
            {
                ObtenerRaiz();
            }
            else
            {
                Console.WriteLine("No tiene soluciones");
            }
            Console.ReadKey();
        }

    }
    public class Principal
    {

        public static void Main(String[] args)
        {
            Raices Ecuacion = new Raices(4, 4, 4);
            Ecuacion.calcular();

        }

    }

}

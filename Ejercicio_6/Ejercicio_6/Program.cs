﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_6
{
    class Libro
    {
        public int ISBN;
        public string titulo;
        public string autor;
        public int numpaginas;

        public Libro(int ISBN, string titulo, string autor, int numpaginas)
        {
            this.ISBN = ISBN;
            this.titulo = titulo;
            this.autor = autor;
            this.numpaginas = numpaginas;
        }

        public int Isbn
        {
            get { return ISBN; }
            set { this.Isbn = ISBN; }
        }
        public int getnumpaginas()
        {
            return numpaginas;
        }
        public void setnumpaginas(int numpaginas)
        {
            this.numpaginas = numpaginas;
        }
        public string AUTOR
        {
            get { return autor; }
            set { this.AUTOR = autor; }
        }
        public string TITULO
        {
            get { return titulo; }
            set { this.TITULO = titulo; }
        }
        public string Informacion()
        {
            return "El libro " + titulo + " con ISBN " + ISBN + ""
                + " creado por el autor " + autor
                + " tiene " + numpaginas + " páginas";
        }

    }
    class Ejecutable
    {
        public static void Main(string[] args)
        {
            Libro libro1 = new Libro(93792532, "IT", "Stephen king", 30);
            Libro libro2 = new Libro(12940234, "harry potter", "Rowling", 60);
            Console.WriteLine(libro1.Informacion());
            Console.WriteLine(libro2.Informacion());
            if (libro1.getnumpaginas() > libro2.getnumpaginas())
            {
                Console.WriteLine(libro1.titulo + " tiene más páginas");
            }
            else
            {
                Console.WriteLine(libro2.titulo + " tiene más páginas");


                Console.ReadKey();
            }


        }
    }
}


    
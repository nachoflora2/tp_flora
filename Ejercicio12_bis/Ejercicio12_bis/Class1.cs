﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio12_bis
{
    class Comercial : Empleado
    {
        private double comision;

        public Comercial(string nombre, int edad, int salario, double comision)
            : base(nombre, edad, salario)
        {
            this.comision = comision;
        }

        public double Comision
        {
            get { return comision; }
            set { this.Comision = comision; }
        }
        public bool plus()
        {

            if (this.Edad > 30 && this.Comision > 200)
            {
                int nuevosalario = salario + PLUS;
                salario = nuevosalario;
                Console.WriteLine("Se le añadido el plus, al empleado " + Nombre);
                return true;
            }

            return false;

        }
    }
    class Repartidor : Empleado
    {
        private string zona;

        public Repartidor(string nombre, int edad, int salario, string zona)
            : base(nombre, edad, salario)
        {
            this.zona = zona;
        }
        public string Zona
        {
            get { return zona; }
            set { this.Zona = zona; }
        }
        public bool plus()
        {

            if (this.Edad < 25 && this.Zona == "zona 3")
            {
                int nuevosalario = salario + PLUS;
                salario = nuevosalario;
                Console.WriteLine("Se le añadido el plus, al empleado " + Nombre);
                return true;
            }

            return false;
        }

    }
}

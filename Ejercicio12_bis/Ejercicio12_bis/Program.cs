﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio12_bis
{
    class Empleado
    {
        private string nombre;
        private int edad;
        public int salario;
        public const int PLUS = 300;

        public Empleado(string nombre, int edad, int salario)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.salario = salario;
        }
        public string Nombre
        {
            get { return nombre; }
            set { this.Nombre = nombre; }
        }
        public int Edad
        {
            get { return edad; }
            set { this.Edad = edad; }
        }
        public int Salario
        {
            get { return salario; }
            set { this.Salario = salario; }
        }
    }

    public class Ejecutable
    {

        public static void Main(string[] args)
        {

            Comercial comerciante1 = new Comercial("Juan", 37, 1000, 300);
            Repartidor repartidor1 = new Repartidor("Fernando", 26, 900, "zona 3");

            Console.WriteLine("{0} {1} {2} {3}", comerciante1.Nombre, comerciante1.Edad, comerciante1.Salario, comerciante1.Comision);
            Console.WriteLine("{0} {1} {2} {3}", repartidor1.Nombre, repartidor1.Edad, repartidor1.Salario, repartidor1.Zona);

            comerciante1.plus();
            repartidor1.plus();

            Console.WriteLine("{0} {1} {2} {3}", comerciante1.Nombre, comerciante1.Edad, comerciante1.Salario, comerciante1.Comision);
            Console.WriteLine("{0} {1} {2} {3}", repartidor1.Nombre, repartidor1.Edad, repartidor1.Salario, repartidor1.Zona);
            Console.ReadLine();
        }

    }
}

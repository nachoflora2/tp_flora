﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    class Cuenta
    {
        public string titular;
        public int cantidad;
        public Cuenta(string titular, int cantidad)
        {
            this.titular = titular;
            this.cantidad = cantidad;
        }
        public string Titular
        {
            get { return titular; }
            set { this.Titular = titular; }
        }
        public int  Cantidad
        {
            get { return cantidad; }
            set { this.Cantidad = cantidad; }
        }
        public void Ingresar(int cantidad)
        {
            if (cantidad > 0)
            {
                cantidad += cantidad;
            }
        }
        public void Retirar(int cantidad)
        {
            if (cantidad - cantidad < 0)
            {
                cantidad = 0;
            }
            else
            {
                cantidad -= cantidad;
            }
        }


    }
   
}

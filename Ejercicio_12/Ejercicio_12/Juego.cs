﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_12
{
    class Juego
    {
        private Jugador[] jugadores;
        private Revolver revolver;

        public Juego(int numjugadores)
        {

            jugadores = new Jugador[comprobarJugadores(numjugadores)];

            crearJugadores();

            revolver = new Revolver();

        }
        private int comprobarJugadores(int numjugadores)
        {

            if (!(numjugadores >= 1 && numjugadores <= 6))
            {
                numjugadores = 6;
            }

            return numjugadores;
        }
        private void crearJugadores()
        {
            for (int i = 0; i < jugadores.Length; i++)
            {
                jugadores[i] = new Jugador(i + 1);
            }
        }
        public bool finjuego()
        {

            for (int i = 0; i < jugadores.Length; i++)
            {
                if (!jugadores[i].Vivo())
                {
                    return true;
                }
            }

            return false;

        }
        public void ronda()
        {

            for (int i = 0; i < jugadores.Length; i++)
            {
                jugadores[i].disparar(revolver);
            }

        }

    }
    public class Ejecutable
    {
        static void Main(string[] args)
        {
            Juego juego = new Juego(1);

            while (!juego.finjuego())
            {
                juego.ronda();
            }

            Console.WriteLine("El juego ha terminado");
            Console.ReadLine();

        }
    }
}

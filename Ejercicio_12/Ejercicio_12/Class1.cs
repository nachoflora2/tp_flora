﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_12
{
    class Jugador
    {
        private int id = 1;
        private string nombre;
        private bool vivo;

        public Jugador(int id)
        {
            this.id = id;
            this.nombre = "Jugador " + id;
            this.vivo = true;
        }

        public void disparar(Revolver r)
        {

            Console.WriteLine("El" + nombre + " se apunta con la pistola");

            if (r.disparar())
            {
                vivo = false;
                Console.WriteLine("El " + nombre + " ha muerto");
            }
            else
            {
                Console.WriteLine("El " + nombre + " sigue vivo");
            }
        }

        public bool Vivo()
        {
            return vivo;
        }

    }

}

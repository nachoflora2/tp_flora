﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_12
{
    class Revolver
    {
        private int posicionactual;
        private int posicionbala;
        Random pos = new Random();
        public Revolver()
        {
            this.posicionactual = pos.Next(1, 7);
            this.posicionbala = pos.Next(1, 7);

        }

        public void siguientebala()
        {

            if (posicionactual == 6)
            {
                posicionactual = 1;
            }
            else
            {
                posicionactual++;
            }

        }


        public bool disparar()
        {

            bool exito = false;

            if (posicionactual == posicionbala)
            {
                exito = true;
            }

            siguientebala();

            return exito;

        }
    }
}
